﻿#region Usings

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Media;

#endregion

namespace PowerShellIseLauncher
{
    public static class Program
    {
        [STAThread]
        public static int Main(string[] args)
        {
            TextOptions.TextFormattingModeProperty.OverrideMetadata(typeof (Visual),
                new FrameworkPropertyMetadata(TextFormattingMode.Display,
                    FrameworkPropertyMetadataOptions.AffectsMeasure |
                    FrameworkPropertyMetadataOptions.AffectsRender |
                    FrameworkPropertyMetadataOptions.Inherits));

            TextOptions.TextRenderingModeProperty.OverrideMetadata(typeof (Visual),
                new FrameworkPropertyMetadata(TextRenderingMode.ClearType,
                    FrameworkPropertyMetadataOptions.AffectsMeasure |
                    FrameworkPropertyMetadataOptions.AffectsRender |
                    FrameworkPropertyMetadataOptions.Inherits));


            string powerShellIseAssembly = Path.Combine(Environment.SystemDirectory, @"WindowsPowerShell\v1.0\PowerShell_ISE.exe");
            Assembly exe = Assembly.LoadFrom(powerShellIseAssembly);
            int returnValue = (int)exe.EntryPoint.Invoke(null, new object[] { args });
            return returnValue;
        }

    }
}